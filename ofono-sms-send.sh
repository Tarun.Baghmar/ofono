#!/bin/sh

set -e

strip_quote() {
	echo "$*" | sed -e "s/.*'\(.*\)'.*/\1/"
}

check_modem(){
    echo "check_modem"
    INTERFACES=$(gdbus call --system --dest org.ofono --object-path "$1" --method org.ofono.Modem.GetProperties)
    for WORD in ${INTERFACES} ; do
       case ${WORD} in 
          "'org.ofono.MessageManager',")
            return 0
       ;;
       esac
    done
    echo "MessageManager not available"
    return 1
}

modem_objectpath_extract(){
   # When two or multiple modems are connected, modem_name regular expression comes in two different way.
   # Following code extract the modem_name by considering two cases. 
   echo "modem_objectpath_extract"
   OBJECTPATH_MODEM=""
   OBJECTPATH=$(gdbus call --system --dest org.ofono --object-path / --method org.ofono.Manager.GetModems)
   if [ -z "${OBJECTPATH}" ]; then
      echo "ObjectPath not available"
      exit 1
   fi
   for WORD in ${OBJECTPATH} ; do
      case ${WORD} in 
	  "'/"*"',"|"('/"*"',")
	    MODEM_PATH=$(strip_quote "${WORD}")
            check_modem "${MODEM_PATH}"
            if [ "$?" = "0" ];
            then
               OBJECTPATH_MODEM=${MODEM_PATH}
               echo "object path going "${MODEM_PATH}""
               break
            else
               echo "Object path of modem not found"
            fi
	    ;;
       esac
   done
}

send_message(){
   echo "send_message"
   modem_objectpath_extract
   if [ -z "${OBJECTPATH_MODEM}" ]; then
      exit 1
   fi
   VALUE=$(gdbus call --system --dest org.ofono --object-path "${OBJECTPATH_MODEM}" --method org.ofono.Modem.SetProperty 'Online' '<true>')
   if [ "${VALUE}" = "()" ]
   then
       OUTPUT=$(gdbus call --system --dest org.ofono --object-path ${OBJECTPATH_MODEM} --method org.ofono.MessageManager.SendMessage "${1}" "${2}")
       for WORD in ${OUTPUT} ; do
         case ${WORD} in
            "'/"*"/message_"*"")
            echo "${WORD}"
         ;;
         esac
       done
   else 
      echo "Not able to set property"
      exit 1
   fi
}

PHONENUMBER=${1}
if [ -z "${PHONENUMBER}" ]; then
   echo "Need to pass phone number"
   exit 1
fi
echo "Phonenumber is ${PHONENUMBER}"

TEXTMESSAGE=${2}
if [ "${TEXTMESSAGE}" = " " ]; then
   echo "Need to pass text to be sent"
   exit 1
fi
echo "Text message to be sent ${TEXTMESSAGE}"

send_message "${PHONENUMBER}" "${TEXTMESSAGE}"
