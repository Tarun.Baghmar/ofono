#!/bin/sh
# vim: set sts=4 sw=4 et tw=80 :
# 

set -e

. common/common-apparmor.sh

#############
# Functions #
#############
sayn() {
    /bin/echo -n -e "\033[01;32m>>>\033[00m $@"
}

say() {
    # Speak in green
    sayn "$@"
    echo
}

########
# Work #
########
START_TIME=$(date +"%F %T")
AA_DUMP="aa-dump_$(date +%Y%m%d-%H%M%S)"
AA_TMPDIR="$(mktemp -d /tmp/apparmor-complaints.XXXXXX)"
TMPDIR="${AA_TMPDIR}/${AA_DUMP}"
mkdir "${TMPDIR}"

sayn "Ready to check for apparmor complaints? [y/N] "
read i
[ "$i" = Y ] || [ "$i" = y ] || exit

say "Checking for apparmor complaints ..."

AUDIT_FILE="${TMPDIR}/audit.log"
journalctl -S "${START_TIME}" -t audit -o cat > ${AUDIT_FILE}

# TODO: There is potential for other processes to cause messages to appear in
#       the journal that may lead to false failures. If this is found to be an
#       issue in practice, then additional filtering of results may be required
apparmor_parse_journal ${AUDIT_FILE} DENIED > "${TMPDIR}/complaint_tokens.log"
if ! [ -s "${TMPDIR}/complaint_tokens.log" ]; then
    say "No complaints found!"
    exit 0
fi
say "Complaints found, creating report ..."

# Collate logs
# The "|| true" is because this is all best-effort: we don't want to
# fail to collect info because of "set -e".
sudo ps aux > "${TMPDIR}/ps_aux.log" || true
sudo journalctl -b > "${TMPDIR}/journalctl.log" || true
uname -a > "${TMPDIR}/uname.log" || true
cp --dereference \
    /etc/image_version \
    /etc/os-release \
    "${TMPDIR}" || true
cp -a /etc/apparmor.d "${TMPDIR}/apparmor.d" \
    > "${TMPDIR}/cp-apparmor-d.log" 2>&1 || true

tar -C "${AA_TMPDIR}" -cvjf ~/"${AA_DUMP}.tar.bz2" "${AA_DUMP}"
say "Report created as $HOME/${AA_DUMP}.tar.bz2"
